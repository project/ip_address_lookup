<?php

namespace Drupal\ip_address_lookup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Ip API settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ip_location_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ip_address_lookup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get the configuration.
    $config = $this->config('ip_address_lookup.settings');

    $form['ip_location_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP Location API Key'),
      '#description' => 'Empty textfield will call the API base path http://ipapi.co/json',
      '#default_value' => $config->get('ip_location_api_key'),
    ];
    $form['ip_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug Mode'),
      '#description' => 'Enable debug mode to check the result for specific ip',
      '#default_value' => $config->get('ip_debug'),
    ];
    $form['ip_debug_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address to Debug'),
      '#description' => 'Provide IP to debug response.',
      '#states' => [
        'visible' => [
          ':input[name="ip_debug"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $config->get('ip_debug_address'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $ip_debug = $form_state->getValue('ip_debug');
    $ip_debug_address = $form_state->getValue('ip_debug_address');
    if(isset($ip_debug) && $ip_debug){
      if(!isset($ip_debug_address) || $ip_debug_address == ''){
        $form_state->setErrorByName('ip_debug_address', $this->t('IP address to Debug field is required')); 
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ip_address_lookup.settings')
      ->set('ip_location_api_key', $form_state->getValue('ip_location_api_key'))
      ->set('ip_debug', $form_state->getValue('ip_debug'))
      ->set('ip_debug_address', $form_state->getValue('ip_debug_address'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
