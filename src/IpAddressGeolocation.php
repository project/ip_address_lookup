<?php

namespace Drupal\ip_address_lookup;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * IpApiGeolocation service.
 */
class IpAddressGeolocation {

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Defines the default configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;


  /**
   * Constructs an IpApiGeolocation object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Defines a factory for logging channels.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_channel_factory, RequestStack $request_stack) {
    $this->config = $config_factory->get('ip_address_lookup.settings');
    $this->logger = $logger_channel_factory->get('ip_address_lookup.getIpAddressGeolocation');
    $this->httpClient = $http_client;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->setUrl();
  }

  /**
   * Sets base URL if the IP API key is empty or not.
   */
  protected function setUrl() {
    $ip_location_api_key = $this->config->get('ip_location_api_key');
    $ip_debug = $this->config->get('ip_debug');
    $ip_debug_address = $this->config->get('ip_debug_address');
    $ip = $this->currentRequest->getClientIp();
    if($ip_debug){
      $ip = $ip_debug_address;
    }
    if ($ip_location_api_key) {
      $this->baseUrl = 'https://ipapi.co/' . $ip . '/json/?key=' . $ip_location_api_key;
    }
    else {
      $this->baseUrl = 'https://ipapi.co/' . $ip . '/json/';
    }
  }

  /**
   * Query the IP Address Lookup API Geolocation.
   *
   * @return \Drupal\ip_address_lookup\IpLocationApiDetails
   *   Return IP Address Lookup API Details Object.
   */
  public function getIpLocationApiDetail() {
    try {
      $response = $this->httpClient->get($this->baseUrl);
      return new IpLocationApiDetails($response->getBody()->getContents());
    }
    catch (ClientException $e) {
      $this->logger->error($e);
      return new IpLocationApiDetails($e->getResponse()->getBody()->getContents());
    }
    catch (\Exception $e) {
      $this->logger->error($e);
    }
  }

}
