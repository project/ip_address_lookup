<?php

namespace Drupal\ip_address_lookup;

/**
 * IpLocationApiDetails service.
 */
class IpLocationApiDetails {

  /**
   * IP Address API Geolocation Country Name.
   *
   * @var string
   */
  protected $countryName;

  /**
   * IP Address API Geolocation Country Code.
   *
   * @var string
   */
  protected $countryCode;

  /**
   * IP Address API Geolocation City.
   *
   * @var string
   */
  protected $city;

  /**
   * IP Address API Geolocation Latitude.
   *
   * @var string
   */
  protected $latitude;

  /**
   * IP Address API Geolocation Longitude.
   *
   * @var float
   */
  protected $longitude;


  /**
   * IP Address API Geolocation Postal.
   *
   * @var string
   */
  protected $postal;

  /**
   * IP Address API Geolocation Timezone.
   *
   * @var string
   */
  protected $timezone;

  /**
   * Region/state short code (FIPS or ISO).
   *
   * @var string
   */
  protected $region;

  /**
   * IP used for the query.
   *
   * @var string
   */
  protected $query;

  /**
   * Constructs an IpLocationApiParameters object.
   *
   * @param string $ip_location_api_service
   *   The IP API service.
   */
  public function __construct(string $ip_location_api_service) {
    $ip_location_api_service = json_decode($ip_location_api_service);
    $this->countryName = $ip_location_api_service->country_name ?? NULL;
    $this->city = $ip_location_api_service->city ?? NULL;
    $this->countryCode = $ip_location_api_service->country_code ?? NULL;
    $this->latitude = $ip_location_api_service->latitude ?? NULL;
    $this->longitude = $ip_location_api_service->longitude ?? NULL;
    $this->postal = $ip_location_api_service->postal ?? NULL;
    $this->timezone = $ip_location_api_service->timezone ?? NULL;
    $this->region = $ip_location_api_service->region ?? NULL;
    $this->query = $ip_location_api_service->query ?? NULL;
  }

  /**
   * Get country name.
   *
   * @return string
   *   Country name(example: United States).
   */
  public function getCountry() {
    return $this->countryName;
  }

  /**
   * Get city.
   *
   * @return string
   *   City name(example: Mountain View).
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * Get two-letter continent code ISO 3166-1 alpha-2.
   *
   * @return string
   *   Two-letter continent code(example: US).
   */
  public function getCountryCode() {
    return $this->countryCode;
  }

  /**
   * Get region/state short code (FIPS or ISO).
   *
   * @return string
   *   Region/state short code (example: CA or 10).
   */
  public function getRegion() {
    return $this->region;
  }

  /**
   * Get latitude.
   *
   * @return float
   *   Latitude (example: 37.386).
   */
  public function getLatitude() {
    return $this->latitude;
  }

  /**
   * Get longitude.
   *
   * @return float
   *   Longitude (example: -122.0838).
   */
  public function getLongitude() {
    return $this->longitude;
  }

  /**
   * Get IP used for the query.
   *
   * @return string
   *   IP (example: 8.8.8.8).
   */
  public function getIp() {
    return $this->query;
  }

  /**
   * Get zip code.
   *
   * @return string
   *   Zip code (example: 94043).
   */
  public function getPostal() {
    return $this->postal;
  }

  /**
   * Get timezone (tz).
   *
   * @return string
   *   Timezone (example: America/Los_Angeles).
   */
  public function getTimezone() {
    return $this->timezone;
  }

}
